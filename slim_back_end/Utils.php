<?php
require_once 'vendor/autoload.php';


class Utils {
 
    private static $instance = null; 
    
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new Utils();
        }
        return static::$instance;
    }

    public function removeNullMemebers($arr) {
        return array_values(array_filter($arr)); //remove null memebers
     }
}