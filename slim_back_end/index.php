<?php
require "bootstrap.php";
require_once "JwtService.php";
require_once "FirebaseService.php";
require_once "Utils.php";

use Final_project\Models\User;

$app = new \Slim\App();

$app->post('/login', function($request, $response, $args){
    $user = $request->getParsedBody();
    $loginResponse = JwtService::getInstance()->login($user['email'], $user['password']);

    return $response->withJson($loginResponse);
 });

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});



$app->get('/users', function($request, $response, $args){ //read
   return query($response, '/users');
 });


$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', /*'http://mysite'*/'*') // * is all the domains
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});


    function query($response, $databasePath) {
    $token = $_GET['token'];
    if (JwtService::getInstance()->validToken($token)) {
        $firebaseService = FirebaseService::getInstance();   
        $database = $firebaseService -> getDatabase();
        $arr = $database->getReference($databasePath)->getValue();
        $arr = Utils::getInstance()->removeNullMemebers($arr);
        return $response->withStatus(200)->withJson($arr);
    } else {
        return $response->withStatus(401);
    }
}


$app->run();
