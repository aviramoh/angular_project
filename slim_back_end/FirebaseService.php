<?php
require_once 'vendor/autoload.php';
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseService {
 
    private static $instance = null; 
    private $firebase;
    private $database;
    
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new FirebaseService();
            static::$instance->initFirebase();
        }
        return static::$instance;
    }

    public function initFirebase() { 
            
    // This assumes that you have placed the Firebase credentials in the same directory
    // as this PHP file.
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/config/firebase_conf.json');

    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->create();

     $this->database =  $firebase->getDatabase();
    }

    public function getFirebase() {
        return $this->firebase;
    }

    public function getDatabase() {
        return $this->database;
    }



    
}