<?php
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

class JwtService {
    // create an empty id variable to hold the user id
    private $id;
    private $name;
    private static $instance = null; 
    
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new JwtService();
        }
        return static::$instance;
    }

    // key for JWT signing and validation
    private $key = "F8F497BAD9F71FCE81F7FC2A7B57B133765EAF309928EA2FCE2A72C68AC2EAA1";

    // create a dummy user for testing without firebase
    private $user = array(
    "id" => 768,
    "name" => "Test User",    
    "email" => "user@test.com",
    "password" => "1234"
    );

    // Checks if the user exists in the database
    private function validUser($email, $password) {
    // doing a user exists check with minimal to no validation on user input
    if ($email == $this->user['email'] && $password == $this->user['password']) {
      // Add user email and id to empty email and id variable and return true
      $this->id = $this->user['id'];
      $this->name = $this->user['name'];
      $this->email = $this->user['email'];
      
      return true;
    } else {
      
      return false;
    }
  }

  //Generates and signs a JWT for User
  private function genJWT() {
    // Make an array for the JWT Payload
    $payload = array(
      "id" => $this->id,
      "name" => $this->name,  
      "email" => $this->email,
      "token" => null
     // "exp" => time() + (60 * 60)
    );
   
    // encode the payload using our secretkey and return the token
    return JWT::encode($payload, $this->key);
  }

  // return signed token if user exists
    public function login($email, $password) {
        $database = FirebaseService::getInstance()->getDatabase();
        $this->user = array_values($database->getReference('/users')
        ->orderByChild('email')
        ->equalTo($email)
        ->getSnapshot()
        ->getValue())[0];
        
    // check if the user exists
    if ($this->validUser($email, md5($password))) {
      // return generate JSON web token
        $this->user['token'] = $this->genJWT();
        return $this->user;
    } else {
      return 'We Couldn\'t Find You In Our Database. Maybe Wrong Email/Password Combination';
    }
  }

  // Validates a given JWT from the user email
  private function validJWT($token) {
    $res = array(false, '');
    // using a try and catch to verify
    try {
      $decoded = JWT::decode($token, $this->key, array('HS256'));
    } catch (Exception $e) {
      return $res;
    }
    $res['0'] = true;
    $res['1'] = (array) $decoded;
 
    return $res;
  }
 
 
  public function validToken($token) {
    if ($token == null) {
        return false;
    } else {
        // checks if the token is valid
        $tokenVal = $this->validJWT($token);
    
        // check if the first array value is true
        return ($tokenVal['0']);
    }
  }

}