import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Subject, BehaviorSubject } from 'rxjs';

const CURRENT_USER: string = 'currentUser';

@Injectable()
export class LoginService {
    currentUser:any;
    loggedIn:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    constructor(private http: HttpClient) {
        
    }
    
    public getCurrentUser() {
        return this.currentUser;
    }

    public isLoggedIn() : BehaviorSubject<boolean> {
        return this.loggedIn;
    }

    public setCurrentUser(user: any) {
        this.currentUser = user;
        this.loggedIn.next(true);
    }
    public login(email: string, password: string) {
        return this.http.post<any>(environment.url + '/login', { email: email, password: password })
            .map(user => {
                if (user && user.token) {
                    this.setCurrentUser(user);
                }

                return user;
            });
    }

    public logout() {
        // remove user from local storage to log user out
        this.currentUser = null;
    }
}