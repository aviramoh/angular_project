import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { AngularFireDatabase, DatabaseQuery } from 'angularfire2/database';
import { Subject, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class PostService {


  filterByUserId: string = null;
  posts$: Subject<Map<string, Post>> = new BehaviorSubject(null);
  posts: any = {};
  applause$: Subject<string> = new BehaviorSubject(null);
  private childChanged: any;
  private childAdded: any;
  postsRef : DatabaseQuery;
  getPostsRef(filterByUserId: string = null): DatabaseQuery {
    if (this.postsRef) {
      this.postsRef.off('child_added');
      this.postsRef.off('child_changed');
      this.postsRef.off('value');
    }
   
    if (filterByUserId) {
      return this.db.database.ref()
        .child("posts")
        .orderByChild('author_uuid')
        .equalTo(filterByUserId)
        .limitToLast(50);
    } else {
      return this.db.database.ref().child("posts").orderByChild('date').limitToLast(50);
    }
  }

  getPosts(filterByUserId: string = null) {
    this.postsRef = this.getPostsRef(filterByUserId);
    this.postsRef.once('value').then((posts) => {
      if (posts) {
        this.posts = posts.val();
        this.posts$.next(this.posts);
      }
    }).catch((error) => {
      console.log(error);
    });

    this.postsRef.on('child_added', (post) => {
        if (this.posts && post) {
          this.posts[post.key] = {};
          this.posts[post.key] = post.val();
        }

        this.posts$.next(this.posts);
      });
    


    this.postsRef.on('child_changed', (post) => {
        if (this.posts && post) {
          if (this.authService.userDetails.uid == post.val().author_uuid
            && post.val().applause && Object.keys(post.val().applause).length > (this.posts[post.key].applause ? Object.keys(this.posts[post.key].applause).length : 0)) {
            this.applause$.next(post.key);
          }

          this.posts[post.key] = {};
          this.posts[post.key] = post.val();
        }
        this.applause$.next(null);
        this.posts$.next(this.posts);
      });
    
  }


  constructor(private authService: AuthService, private db: AngularFireDatabase) {
  }

  postMessage(newPost: Post) {
    return this.db.database.ref().child("posts").push().set(newPost);
  }

  postComment(postId: string, comment: Post) {
    return this.db.database.ref().child("posts").child(postId).child("comments").push().set(comment);
  }

  applause(postId: string, uuid: string) {
    return this.db.database.ref().child("posts").child(postId).child("applause").child(uuid).set(Date.now());
  }

}
