import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

@Injectable()
export class LoginComponent implements OnInit {
  authService: AuthService;
  router: Router;
  user = {
    email: '',
    password: ''
  };
  response : string;

  constructor(router: Router, authService: AuthService) { 
    this.authService = authService;
    this.router = router;
  }


  signInWithEmail() {
    this.authService.signInRegular(this.user.email, this.user.password)
       .then((res) => {
          console.log(res);
          this.router.navigate(['']);
       })
       .catch((err) => {
        this.response = err; 
        console.log('error: ' + err)}
      );
 }

  ngOnInit() {
    this.authService.isLoggedIn().subscribe((isLoggedIn) => {
      if (isLoggedIn) {
        this.router.navigate(['']);
      }    
    });
  }



}