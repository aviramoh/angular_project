import { environment } from './../../environments/environment';
import { LoginService } from './../services/login/login.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class FirebaseService {
  constructor(private db:AngularFireDatabase, private http:HttpClient, private loginService:LoginService) { }

  getUsersFire(){
    return this.http.get(environment.url + "/users?token=" + this.loginService.getCurrentUser().token);
  
  }
}
