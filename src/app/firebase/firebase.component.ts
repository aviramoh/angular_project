import { FirebaseService } from './firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.css']
})
export class FirebaseComponent implements OnInit {

  constructor(private service:FirebaseService) { }

  users;

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
      console.log(response);
      this.users = response;
    });
  }

}
