import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user';
import { Post } from '../models/post';
import { PostService } from '../services/post.service';
import { UserService } from '../services/user.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  userInfo: User;
  applauseAudio: any;
  newPost: Post = new Post();
  comment: Post = new Post();
  posts: any = {};
  postKeys: string[] = [];
  users: any = {};
  filterByUserId: string;
  applauseSubscriber: any;
  postsSubscriber: any;
  private applauseUrl : string;
  constructor(private userService: UserService, private db:AngularFireDatabase, private route: ActivatedRoute, private postService: PostService, private authService: AuthService) {
    this.authService.userInfo.subscribe((userInfo) => {
      this.userInfo = userInfo;
    });
    this.route.params.subscribe((params) => {
      this.filterByUserId = params['userId'];
      this.postService.getPosts(this.filterByUserId);
      this.getPosts();
      this.getUsers();
    });
  }

  ngOnInit() {
    this.db.app.storage().ref().child("audio/applause.mp3").getDownloadURL().then((url) => {
      this.applauseUrl = url;
      this.applauseAudio = new Audio(this.applauseUrl);
      this.applauseAudio.load();
    }).catch((error) => {
      console.log(error);
    });

  }


  public getPosts() {
    if (!this.applauseSubscriber) {
      console.log("applauseSubscriber");
      this.applauseSubscriber = this.postService.applause$.subscribe((applause) => {
        if (applause) {         
          this. applauseAudio.play();
          console.log("applause");
        }
      });
    }

    if (!this.postsSubscriber) {
      console.log("postsSubscriber");
      this.postsSubscriber = this.postService.posts$.subscribe((posts) => {
        if (posts) {
          this.posts = posts;
          this.postKeys = Object.keys(posts);
        } else {
          this.posts = {};
          this.postKeys = [];
        }
      });
    }
  }

  public getUsers() {
    this.userService.users$.subscribe((users) => {
      if (users) {
        this.users = users;
      }
    });
  }

  getCurrentUserId() {
    return this.authService.userDetails.uid;
  }


  public postMessage(newPost: Post) {
    newPost.date = Date.now();
    newPost.author_uuid = this.authService.userDetails.uid;
    this.postService.postMessage(newPost).then((data) => {
      this.newPost = new Post();
    }).catch((error) => {
      console.log(error);
    });
  }

  public postComment(postId: string) {
    let comment: Post = new Post();
    comment.content = this.posts[postId].comment;
    comment.date = Date.now();
    comment.author_uuid = this.authService.userDetails.uid;
    this.postService.postComment(postId, comment).then((data) => {

    }).catch((error) => {
      console.log(error);
    });
  }

  public addApplause(postId: string) {
    let post = this.posts[postId];
    this.postService.applause(postId, this.authService.userDetails.uid).then((data) => {
    }).catch((error) => {
      console.log(error);
    });
  }

  getJsonValues(json: any) {
    if (json) {
      return Object.values(json);
    }
    return [];
  }

  getJsonKeys(json: any) {
    if (json) {
      return Object.keys(json);
    }
    return [];
  }

}
