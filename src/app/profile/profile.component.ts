import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private authService: AuthService, private userService: UserService, private route: ActivatedRoute, private router: Router) { }
  userInfo: User;
  userUid: string = null;
  followersCount: number = 0;
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.userUid = params['userId'];
      this.userService.addFollower(this.userUid, this.authService.userDetails.uid).then((data) => {
        this.userService.getUserInfo(this.userUid).on('value', (userFromDb) => {
          this.userInfo = userFromDb.val();
          this.followersCount = Object.keys(this.userInfo.followers).length;
          this.followersCount = this.followersCount ? this.followersCount : 0;
        });
      });

    });

  }
}


