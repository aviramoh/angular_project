import { HttpClient } from '@angular/common/http';
import { LoginService } from './services/login/login.service';
import { FirebaseService } from './firebase/firebase.service';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { FirebaseComponent } from './firebase/firebase.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { RegistrationComponent } from './registration/registration.component';
import * as firebase from 'firebase';
import { UserService } from './services/user.service';
import { PostComponent } from './post/post.component';
import { PostService } from './services/post.service';
import { ProfileComponent } from './profile/profile.component';

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    FirebaseComponent,
    LoginComponent,
    NavigationComponent,
    NotFoundComponent,
    RegistrationComponent,
    PostComponent,
    ProfileComponent,
    
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'Ortal And Aviram Social Network'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'', component:PostComponent, canActivate: [AuthGuardService]},
      {path:'login', component:LoginComponent},      
      {path:'registration', component:RegistrationComponent},
      {path: 'profile/:userId', component: ProfileComponent, canActivate: [AuthGuardService]}, 
      {path:'**', component:NotFoundComponent}//have to be last
    ]),
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    LoginService,
    FirebaseService,
    AuthService,
    AuthGuardService,
    UserService,
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
