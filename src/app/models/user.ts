export class User {
    name: string;
    mail: string;
    password: string;
    gender: string;
    imgPath: string;
    age: number;
    city: string;
    followers: any;
}
