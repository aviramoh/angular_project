import { Timestamp } from "rxjs";

export class Post {
    author_uuid: string;
    date: number;
    content: Text;
    comments_post_uuid: number[];
    comments: any;
    applause: Map<string, boolean> = new Map();
}
